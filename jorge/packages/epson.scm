(define-module (jorge packages epson)
  #:use-module (gnu packages)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages cpio)
  #:use-module (gnu packages image)
  #:use-module (gnu packages base)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (nonguix build-system binary)
  #:use-module (srfi srfi-9))

;; I've made my own packages to make things easier (I think)
;; someday I'll make this work out from the original RPM.
;; Hopefully with all ESC/P printers.

(define-record-type <license>
  (license name uri comment)
  license?
  (name    license-name)
  (uri     license-uri)
  (comment license-comment))

(define epson-license
  ;; NON-FREE LICENSE (I should use nonguix non-free stuff instead of this)
  (license
   "Seiko EPSON Corporation Software License Agreement"
   "https://download.ebz.epson.net/la/linux/injet_for_linux.html"
   "https://download.ebz.epson.net/dsc/du/02/eula/global/LINUX_EN.html"))

(define-public epson-inkjet-printer-filter
  ;; Non-free though it does not have binary blobs
  (package
    (name "epson-inkjet-printer-filter")
    (version "1.0.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://gitlab.com/jorge215/"
                                  "epson-esc-p-cups-drivers/-/raw/master/"
                                  "epson-inkjet-printer-filter-1.0.0.tar.gz"))
              (sha256
               (base32 "15nnc8scbm9yvj7zz81wq4v4xbppy1i12rn5x61651bic08g3cvp"))))
    (build-system gnu-build-system)
    (arguments
     `(#:configure-flags
       `(,(string-append "--prefix="
                         (assoc-ref %outputs "out"))
         ,(string-append "--with-cupsfilterdir="
                         (assoc-ref %outputs "out") "/lib/cups/filter")
         ,(string-append "--with-cupsppddir="
                         (assoc-ref %outputs "out") "/share/ppd"))))
    (inputs `(("cups-minimal" ,cups-minimal)
              ("ghostscript" ,ghostscript)
              ("libjpeg-turbo" ,libjpeg-turbo)))
    (synopsis "EPSON ESC/P CUPS Printer filter")
    (description "Non-free ESC/P CUPS Printer filter required for some EPSON printers")
    (home-page "https://download.ebz.epson.net/dsc/search/01/search/?OSC=LX")
    (license epson-license)))

(define-public epson-inkjet-printer-201207w
  ;; Non-free, also it has binary blobs
  (package
    (name "epson-inkjet-printer-201207w")
    (version "1.0.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://gitlab.com/jorge215/"
                                  "epson-esc-p-cups-drivers/-/raw/master/"
                                  "epson-inkjet-printer-201207w-1.0.0_x86_64.tar.gz"))
              (sha256
               (base32 "18vcygmzh5nh4a7n7imjvc69xfygqi7g1amq6cir2qwv6lx0vp3p"))))
    (build-system binary-build-system)
    (arguments
     `(#:strip-binaries? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'patchelf 'set-ppds-prefix
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out"))
                   (in (assoc-ref inputs "epson-inkjet-printer-filter")))
               (system
                (string-append "sed -i 's|%PREFIX%/lib|" in "/lib|g' ppds/*"))
               (system
                (string-append "sed -i 's|%PREFIX%|" out "|g' ppds/*")))
             #t)))
       #:patchelf-plan
       `(("lib/libEpson_201207w.MT.so.1.0.0"
          ("glibc" "out"))
         ("lib/libEpson_201207w.so.1.0.0"
          ("glibc" "out")))
       #:install-plan
       `(("lib" "./")
         ("include" "./")
         ("resource" "./")
         ("watermark" "./")
         ("ppds/*" "/share/ppd/"))))
    (inputs
     `(("glibc" ,glibc)))
    (native-inputs
     `(("epson-inkjet-printer-filter" ,epson-inkjet-printer-filter)))
    (synopsis "EPSON ESC/P CUPS PPD files (release 201207w)")
    (description "Non-free EPSON ESC/P CUPS PPD files for L110 Series,
L210 Series, L300 Series, L350 Series, L355 Series, L550 Series, L555 Series")
    (home-page "https://download.ebz.epson.net/dsc/search/01/search/?OSC=LX")
    (license epson-license)))
